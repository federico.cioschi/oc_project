from oc_project.data_acquisition import data_acquisition
from oc_project.data_saving.data_saving import pickle_load, pickle_dump
from oc_project.benchmarks import benchmarks, plotting
from oc_project.shortest_path import preprocessing
import argparse
import os


def get_DC_graph_and_coord():
    URL_DC = "http://www.diag.uniroma1.it//~challenge9/data/tiger/DC.tmp.gz"
    return data_acquisition.download_and_build_graph(URL_DC, data_acquisition.TIGER_builder)


def launch_preprocessing(out_path):
    G, _ = get_DC_graph_and_coord()
    results = preprocessing.preprocess(G, 300, 2)
    pickle_dump(results, out_path)


def launch_benchmarking(in_path, out_path):
    G, coord = get_DC_graph_and_coord()
    preprocessing_results = pickle_load(in_path)

    print("[+] Benchmarking.... ")
    print("[+] Graph has {} nodes and {} arcs".format(len(G.nodes), len(G.edges)))

    global_benchmarks_results = benchmarks.benchmarks(G, preprocessing_results, coord)

    pickle_dump(global_benchmarks_results, out_path)


def launch_plotting(in_path, out_path):
    ALGOS = [
        ('bid_dijkstra', 'Bidirectional Dijkstra'),
        ('bid_astar', 'Bidirectional A*'),
        ('bid_dijkstra_with_preprocessing', 'Bidirectioanl Dijkstra with preprocessing'),
        ('bid_astar_with_preprocessing', 'Bidirectional A* with preprocessing')
    ]

    global_benchmarks_results = pickle_load(in_path)

    plotting.plot_graphs(ALGOS, global_benchmarks_results, out_path)


def manage_arguments():
    parser = argparse.ArgumentParser(description='Combinatorial Optimization Project')

    subparsers = parser.add_subparsers(required=True, dest='command')

    preprocessing_parser = subparsers.add_parser( 'preprocessing', help='Launch preprocessing')
    preprocessing_parser.add_argument('-o', '--out', type=str, required=True, dest='out_path', metavar='PATH', help='File path where you want to save preprocessing results.')

    benchmarking_parser = subparsers.add_parser( 'benchmarking', help='Launch benchmarking')
    benchmarking_parser.add_argument('-i', '--in',  type=str, required=True, dest='in_path', metavar='PATH', help='File path containing preprocessing results.')
    benchmarking_parser.add_argument('-o', '--out', type=str, required=True, dest='out_path', metavar='PATH', help='File path where you want to save benchmarking results.')

    benchmarking_parser = subparsers.add_parser( 'plotting', help='Plot graphs about benchmarking')
    benchmarking_parser.add_argument('-i', '--in',  type=str, required=True, dest='in_path', metavar='PATH', help='File path containing benchmarking results.')
    benchmarking_parser.add_argument('-o', '--out', type=str, required=True, dest='out_path', metavar='PATH', help='Directory path where you want to save graphs.')

    return parser.parse_args()


def main():
    args = manage_arguments()

    if args.command=='preprocessing':
        launch_preprocessing(args.out_path)
    elif args.command=='benchmarking':
        launch_benchmarking(args.in_path, args.out_path)
    elif args.command=='plotting':
        launch_plotting(args.in_path, args.out_path)
    else:
        print("[+] {} command not supported!".format(args.command))
        os.sys.exit(1)


if __name__ == '__main__':
    main()