import networkx as nx
from matplotlib import pyplot as plt

def build_G1():
    G = nx.DiGraph()
    G.add_weighted_edges_from([
     (1, 2, 1),
     (2, 3, 2),
     (3, 4, 3),
     (4, 5, 2),
     (1, 6, 5),
     (6, 7, 4),
     (7, 3, 4), #(7, 3, 3),
     (7, 4, 5),
     (8, 2, 4),
     (2, 8, 7),
     (8, 10, 4),
     (8, 9, 6),
     (9, 3, 7),
     (9, 5, 4),
     (3, 9, 4),
     (10, 9, 4),
     (1, 8, 4),
     (3, 5, 6),
     (2, 7, 4),
     (6, 2, 5)         
    ])
    return G
    
def plot_G1():
    G = build_G1()
    nodes_pos = {
        1: (0,3),
        2: (1,2),
        3: (1,1),
        4: (2,1),
        5: (2,0),
        6: (2,3),
        7: (2,2),
        8: (0,2),
        9: (1,0),
        10: (0,0)
    }
    edges_weights=nx.get_edge_attributes(G,'weight')
    nx.draw_networkx(G, nodes_pos, node_size=450, node_color='w', edgecolors='k')
    nx.draw_networkx_edges(G, nodes_pos)
    nx.draw_networkx_edge_labels(G, nodes_pos, edge_labels=edges_weights)
    plt.axis('off')
    plt.show()


def build_G2():
    "Graph used to test getLines()"
    G2 = nx.DiGraph()
    G2.add_edges_from([
        (1,2),
        (2,3),
        (3,9),
        (9,4),
        (4,5),
        (5,6),
        (6,7),
        (1,10),
        (10,13),
        (4,8),
        (8,3),
        (10,11),
        (11,12),
        (12,7),
        (12,11),
        (11,10),
        (13,14),
        (14,15),
        (15,16),
        (16,7),
        (16,15),
        (15,14),
        (14,13),

        (1,17),
        (17,18),
        (18,19),
        (19,20),
        (20,21),
        (21,22),
        (22,7),
        (13,23),
        (7,28),
        (23,24),
        (24,25),
        (25,26),
        (26,27),
        (27,28),
        (28,27),
        (27,26),
        (26,25),
        (25,24),
        (24,23)
    ])
    return G2

def build_G3():
    "Graph used to test grow_partial_tree"
    G = nx.DiGraph()
    G.add_weighted_edges_from([
        (1,2,5),
        (2,3,1),
        (3,4,2),
        (4,5,1),
        (5,6,2),
        (6,7,3),
        (7,14,1),

        (1,8,10),
        (8,9,1),
        (9,10,1),
        (10,11,1),
        (11,12,1),
        (12,13,4),
        (13,15,1)
    ])
    return G

def build_G4():
    "Used to test partial_tree_step"
    G = nx.DiGraph()
    G.add_weighted_edges_from([
        (1,2,2),
        (2,3,1),
        (3,4,3),
        (4,5,1),
        (5,6,2),
        (6,7,1),
        (7,8,2)
    ])
    return G

def build_G5():
    "used to test shortcut-step"
    G = nx.DiGraph()
    G.add_weighted_edges_from([
        (1,2,1),
        (2,3,2),
        (3,4,3),
        (4,5,2),
        (5,6,1),
        (6,7,2),
        (1,8,2),
        (8,9,1),
        (9,8,1),
        (9,10,2),
        (10,9,2),
        (10,11,3),
        (11,10,2),
        (11,12,2),
        (12,11,1),
        (12,7,1)
    ])
    return G
    
if __name__ == "__main__":
    plot_G1()