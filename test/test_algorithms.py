from shortest_path.algorithms import bidirectional_Astar
import unittest
import networkx as nx
from oc_project.shortest_path import algorithms
from oc_project.test import predefined_graphs

class AlgorithmsTest(unittest.TestCase):
    
    def _test_algo_generic(self, G, algo):
        for s in G.nodes:
            for t in G.nodes:
                if s!=t and nx.has_path(G, s, t):
                    self.assertEqual(
                        nx.shortest_path(G, source=s, target=t, weight='weight', method='dijkstra'),
                        algo(G, s, t) if algo!=algorithms.bidirectional_Astar else algo(G, s, t)['shortest_path']
                    )
    
    def test_dijkstra_C(self):
        G = predefined_graphs.build_G1()
        self._test_algo_generic(G, algorithms.dijkstra_C)
        
    def test_dijkstra_source2target(self):
        G = predefined_graphs.build_G1()
        self._test_algo_generic(G, algorithms.dijkstra_source2target)
    
    def test_Astar(self):
        G = predefined_graphs.build_G1()
        self._test_algo_generic(G, algorithms.Astar)
    
    def test_Astar_bidirectional(self):
        G = predefined_graphs.build_G1()
        self._test_algo_generic(G, algorithms.bidirectional_Astar)


if __name__ == '__main__':
    unittest.main()