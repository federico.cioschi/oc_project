from shortest_path.util import get_arcs

import networkx as nx

def calculate_exact_arc_reaches(G):
    """Given a graph `G`, this function calculates exact values for arc reaches.
    Calculation is strictly adherent to definition given at the beginning of
    section 5.2.1 of the technical report.
    This function, used only for testing purposes; it uses the `networkx`
    library's algorithms for shortest path search.
    """

    def path_len(path):
        res = 0
        for (u,v) in get_arcs(path):
            res += G[u][v]['weight']
        return res

    arc_reaches = {arc:0 for arc in G.edges}

    shortest_paths = nx.shortest_path(G, weight='weight', method='dijkstra')

    for source in G.nodes:
        if source not in shortest_paths:
            continue
        for target in G.nodes:
            if target not in shortest_paths[source]:
                continue
            path = shortest_paths[source][target]
            for (u,v) in get_arcs(path):
                path_arc_reach = min(path_len(shortest_paths[source][v]), path_len(shortest_paths[u][target]))
                if path_arc_reach > arc_reaches[(u,v)]:
                    arc_reaches[(u,v)] = path_arc_reach
    return arc_reaches

