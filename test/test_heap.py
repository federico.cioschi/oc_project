import unittest
import random

from shortest_path.heap import BinaryHeap

class HeapTest(unittest.TestCase):

    def test_trivial(self):
        heap = BinaryHeap()
        heap.insert(1, 5)
        heap.insert(2, 3)
        heap.insert(3, 20)
        heap.insert(4, 7)
        self.assertEqual(heap.extractMin(), (2,3))
    
    def test_insert(self):
        random.seed(42)
        heap = BinaryHeap()
        minNode, minValue = None, float('inf')
        for node in range(1, 100+1):
            value = random.randint(1, 10**6)
            if value<minValue:
                minNode, minValue = node, value
            heap.insert(node, value)
        self.assertEqual(heap.extractMin(), (minNode, minValue))
        
    def test_decreaseKey(self):
        heap = BinaryHeap()
        heap.insert(1, 5)
        heap.insert(2, 3)
        heap.insert(3, 20)
        heap.insert(4, 7)
        self.assertEqual(heap.extractMin(), (2,3))
        
        heap.decreaseKey(3, 2)
        self.assertEqual(heap.extractMin(), (3,2))
    
    def test_multi_extractMin(self):
        N = 100
        random.seed(42)
        heap = BinaryHeap()
        ll = list()
        for node in range(1, N+1):
            value = random.randint(1, 10**6)
            ll.append((node, value))
            heap.insert(node, value)
        list_from_heap = [heap.extractMin() for _ in range(1, N+1)]
        ll.sort(key=lambda e: e[1])
        
        self.assertEqual(ll, list_from_heap)


if __name__ == '__main__':
    unittest.main()