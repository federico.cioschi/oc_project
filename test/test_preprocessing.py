import unittest
import networkx as nx

from test import predefined_graphs
from test.util import calculate_exact_arc_reaches
from shortest_path import preprocessing
from tqdm import tqdm
from functools import partialmethod

class PreProcessingTest(unittest.TestCase):

    def setUp(self):
        tqdm.__init__ = partialmethod(tqdm.__init__, disable=True)


    def test_grow_partial_tree(self):
        """
        Tests on a simple case if `grow_partial_tree` procedure put the right elements
        in the inerr_circle and in the outer_circle.

        This test employs graph G3:
        ![Graph G3]()
        """
        G = predefined_graphs.build_G3()
        results = preprocessing.grow_partial_tree(G, 1, 3)

        self.assertSetEqual(set([1,2,3,4,8,9,10,11]), results["inner_circle"])
        for outer_node in results["outer_circle"]:
            self.assertIn(outer_node, set([5,6,7,14,12,13,15]))


    def test_grow_partial_tree_stopping_criterion(self):
        """Tests `grow_partial_tree` procedure's stopping criterion on graph G1:
        ![Graph G1]()
        """
        G1 = predefined_graphs.build_G1()
        epsilon = 5

        for source in G1.nodes():
            results = preprocessing.grow_partial_tree(G1, source, epsilon)

            labeled = results['labeled']
            inner_circle = results['inner_circle']
            outer_circle = results['outer_circle']
            pred = results['pred']
            c = lambda i, j: G1[i][j]['weight']

            def dist_from_inner_circle(j):
                d = 0
                while j not in inner_circle:
                    i = pred[j]
                    d += c(i, j)
                    j = i
                return d

            for labeled_node in labeled:
                self.assertIn(labeled_node, outer_circle)
                self.assertGreater(dist_from_inner_circle(labeled_node), epsilon)


    def test_grow_partial_tree_shortest_path_search(self):
        """Test if `grow_partial_tree` procedure works also as a normal shortest path
        searching algorithm.

        This test emplys graph G1:
        ![Graph G1]()"""

        from shortest_path.algorithms import build_path

        G1 = predefined_graphs.build_G1()
        effective_shortest_paths = nx.shortest_path(G1, weight='weight', method='dijkstra')

        for source in G1.nodes():
            results = preprocessing.grow_partial_tree(G1, source, float('inf'))
            for target in G1.nodes():
                if nx.has_path(G1, source, target):
                    calculated_path = build_path(results['pred'], source, target)
                    self.assertListEqual(effective_shortest_paths[source][target],
                                         calculated_path)


    def test_grow_partial_tree_circles_conditions(self):
        """Tests if members of the inner circle and members of the outer circle respect
        the following conditions:

            - given the inner node `v`: `d(v)-l(x, f(v)) <= epsilon`
            - given the outer node `v`: `d(v)-l(x, f(v)) >  epsilon`

        where `x` is the root of the calculated shorttest path tree.

        This test employs graph G1:
        ![Graph G1]()"""

        epsilon = 5
        G1 = predefined_graphs.build_G1()

        for source in G1.nodes():
            results = preprocessing.grow_partial_tree(G1, source, epsilon)
            inner_circle = results['inner_circle']
            outer_circle = results['outer_circle']
            l = lambda i, j: G1[i][j]['weight']
            d = results['dist']
            f = results['f']
            for v in inner_circle:
                if v!=source:
                    self.assertLessEqual(d[v] - l(source, f[v]), epsilon)
            for v in outer_circle:
                self.assertGreater(d[v] - l(source, f[v]), epsilon)


    def test_create_tree(self):
        """
        Tree used in the test:
            3
            ├── 2
            │   ├── 0
            │   └── 1
            ├── 4
            │   ├── 5
            │   │   ├── 6
            │   │   └── 7
            │   └── 8
            └── 9
                ├── 10
                └── 11
        """
        pred = {0:2, 1:2, 2:3, 4:3, 5:4, 6:5, 7:5, 8:4, 9:3, 10:9, 11:9}
        tree = preprocessing.create_tree(3, pred)
        self.assertEqual(3, tree.root)
        self.assertEqual(12, len(tree.nodes))
        self.assertEqual(3, tree.depth())
        self.assertSetEqual(set(range(12)), set(tree.nodes.keys()))
        self.assertSetEqual(set([1,0,10,11,8,7,6]),
                            set(node.identifier for node in tree.leaves())
                        )
        self.assertEqual(5, tree.parent(6).identifier)
        self.assertEqual(4, tree.parent(5).identifier)


    def __test_partial_tree_step_upper_bounds(self, G):
        exact_arc_reaches = calculate_exact_arc_reaches(G)

        epsilon = 1

        upper_bound_arc_reaches = {arc:0 for arc in G.edges}
        deleted_arcs = set()

        current_G = G.copy()
        while len(current_G.edges)>0:
            preprocessing.partial_tree_step(G, current_G, upper_bound_arc_reaches, deleted_arcs, epsilon)
            epsilon *= 2

        for arc in G.edges:
            self.assertGreaterEqual(upper_bound_arc_reaches[arc], exact_arc_reaches[arc])


    def test_partial_tree_step_upper_bounds_on_G1(self):
        self.__test_partial_tree_step_upper_bounds(predefined_graphs.build_G1())


    def test_partial_tree_step_upper_bounds_on_G4(self):
        self.__test_partial_tree_step_upper_bounds(predefined_graphs.build_G4())


    def __test_partial_tree_step_big_epsilon(self, G):
        exact_arc_reaches = calculate_exact_arc_reaches(G)

        epsilon = 100

        upper_bound_arc_reaches = {arc:0 for arc in G.edges}
        deleted_arcs = set()

        newG = G.copy()
        newG = preprocessing.partial_tree_step(G, newG, upper_bound_arc_reaches, deleted_arcs, epsilon)

        for arc in G.edges:
            self.assertEqual(upper_bound_arc_reaches[arc], exact_arc_reaches[arc])


    def test_partial_tree_step_big_epsilon_on_G1(self):
        self.__test_partial_tree_step_big_epsilon(predefined_graphs.build_G1())


    def test_partial_tree_step_big_epsilon_on_G4(self):
        self.__test_partial_tree_step_big_epsilon(predefined_graphs.build_G4())


    def test_get_lines(self):
        G = predefined_graphs.build_G2()
        lines_1way, lines_2way = preprocessing.get_lines(G)
        effective_lines_1way = [ [1, 2, 3],
                                  [4, 5, 6, 7],
                                  [4, 8, 3],
                                  [3, 9, 4],
                                  [1, 17, 18, 19, 20, 21, 22, 7]
                                ]
        effective_lines_2way = [[12, 11, 10],
                                [16, 15, 14, 13],
                                [28, 27, 26, 25, 24, 23]
                               ]
        
        self.assertEqual(len(effective_lines_1way), len(lines_1way))
        for line_1way in effective_lines_1way:
            self.assertTrue(line_1way in effective_lines_1way)
        
        self.assertEqual(len(effective_lines_2way), len(lines_2way))
        for line_2way in effective_lines_2way:
            self.assertTrue(line_2way in effective_lines_2way or
                            line_2way[::-1] in effective_lines_2way)


    def test_get_closer_to_median_index(self):
        G = predefined_graphs.build_G4()

        self.assertEqual(2, preprocessing.get_closer_to_median_index(G, [1,2,3,4]))
        self.assertEqual(2, preprocessing.get_closer_to_median_index(G, [1,2,3,4,5]))
        self.assertEqual(3, preprocessing.get_closer_to_median_index(G, [1,2,3,4,5,6,7]))
        self.assertEqual(3, preprocessing.get_closer_to_median_index(G, [1,2,3,4,5,6,7,8]))
        self.assertEqual(1, preprocessing.get_closer_to_median_index(G, [3,4,5,6]))
        self.assertEqual(2, preprocessing.get_closer_to_median_index(G, [3,4,5,6,7,8]))


    def test_get_closer_to_median_two_way(self):
        G = predefined_graphs.build_G4()
        G.add_weighted_edges_from([
            (2,1,4),
            (3,2,1),
            (4,3,3),
            (5,4,2)
        ])

        self.assertEqual(1, preprocessing.get_closer_to_median_index(G, [1,2,3,4], two_way=True))
        self.assertEqual(2, preprocessing.get_closer_to_median_index(G, [1,2,3,4,5], two_way=True))


    def test_shortcuts_creation_with_big_lambda(self):
        """Test if shortcuts are properly created and with the right weight.
        The two_way line of G5 was specially build to have paths of different
        lengths in each direction.

        Moreover, it tests the deletion of bypassed arcs and vertices.
        """
        G = predefined_graphs.build_G5()

        current_G = G.copy()
        deleted_arcs = set()
        arc_reaches_upper_bounds = {arc:0 for arc in G.edges}
        shortcuts = dict()

        _lambda = 15

        preprocessing.shortcut_step(G, current_G, arc_reaches_upper_bounds, deleted_arcs, shortcuts, _lambda)

        self.assertIn((1,7), current_G.edges)
        self.assertEqual(current_G[1][7]['weight'], 11)
        for u in [1,2,3,4,5,6,7]:
            for v in [1,2,3,4,5,6,7]:
                if (u,v)!=(1,7) and u!=v:
                    self.assertNotIn((u,v), current_G.edges)
        for v in [2,3,4,5,6]:
            self.assertNotIn(v, current_G.nodes)

        self.assertIn((8,12), current_G.edges)
        self.assertEqual(current_G[8][12]['weight'], 8)
        self.assertIn((12,8), current_G.edges)
        self.assertEqual(current_G[12][8]['weight'], 6)
        for u in [8,9,10,11,12]:
            for v in [8,9,10,11,12]:
                if (u,v)!=(8,12) and (u,v)!=(12,8) and u!=v:
                    self.assertNotIn((u,v), current_G.edges)
        for v in [9,10,11]:
            self.assertNotIn(v, current_G.nodes)


    def test_shortcuts_creation_with_small_lambda(self):
        """Test shortcuts creation when shortucts' length cannot exceed
        given lambda parameter.
        """
        G = predefined_graphs.build_G5()

        current_G = G.copy()
        deleted_arcs = set()
        arc_reaches_upper_bounds = {arc:0 for arc in G.edges}
        shortcuts = dict()

        _lambda = 7

        preprocessing.shortcut_step(G, current_G, arc_reaches_upper_bounds, deleted_arcs, shortcuts, _lambda)

        self.assertNotIn((1,7), current_G.edges)
        self.assertIn((1,4), current_G.edges)
        self.assertEqual(current_G[1][4]['weight'], 6)
        self.assertIn((4,7), current_G.edges)
        self.assertEqual(current_G[4][7]['weight'], 5)

        self.assertNotIn((8,12), current_G.edges)
        self.assertIn((8,10), current_G.edges)
        self.assertEqual(current_G[8][10]['weight'], 3)
        self.assertIn((10,8), current_G.edges)
        self.assertEqual(current_G[10][8]['weight'], 3)
        self.assertIn((10,12), current_G.edges)
        self.assertEqual(current_G[10][12]['weight'], 5)
        self.assertIn((12,10), current_G.edges)
        self.assertEqual(current_G[12][10]['weight'], 3)


    def test_arc_reaches_bounds_in_shortcut_step(self):
        """During shortcuts addition, bypassed arcs are removed from `current_G`
        and their reach upper bound is estimated. These estimates refers to
        the reach of bypassed arcs in the orginal graph, after the addition
        of shortucuts. So, this test verifies if in `G_with_shortcuts` the 
        estimates really represent an upper bound over exact arc reaches.
        """
        G = predefined_graphs.build_G5()

        current_G = G.copy()
        deleted_arcs = set()
        arc_reaches_upper_bounds = {arc:0 for arc in G.edges}
        shortcuts = dict()

        _lambda = 15

        preprocessing.shortcut_step(G, current_G, arc_reaches_upper_bounds, deleted_arcs, shortcuts, _lambda)

        # Add shortcuts to the original graph G
        G_with_shortcuts = G.copy()
        for (u,w) in shortcuts:
            weight, _ = shortcuts[(u,w)]
            G_with_shortcuts.add_edge(u, w, weight=weight)

        actual_arc_reaches = calculate_exact_arc_reaches(G_with_shortcuts)

        for arc in deleted_arcs:
            self.assertGreaterEqual(arc_reaches_upper_bounds[arc], actual_arc_reaches[arc],
            msg="arc: {}".format(arc))


    def test_circular_lines_elimination(self):

        def build_circle(G, start, circle_length, two_way=False):
            for i in range(start, start+circle_length-1):
                G.add_edge(i, i+1, weigth=1)
                if two_way:
                    G.add_edge(i+1, i, weigth=1)
            G.add_edge(start+circle_length-1, start, weigth=1)
            if two_way:
                G.add_edge(start, start+circle_length-1, weigth=1)

        G = nx.DiGraph()

        build_circle(G, 1, 4)
        build_circle(G, 5, 4, two_way=True)

        lines_1way, lines_2way = preprocessing.get_lines(G)

        self.assertEqual([], lines_1way)
        self.assertEqual([], lines_2way)


if __name__ == '__main__':
    unittest.main()