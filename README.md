# Combinatorial Optimization Project

## Install
```sh
pip install -r oc_project/requirements.txt
```

## Usage
```sh
$ python -m oc_project.main -h
usage: main.py [-h] {preprocessing,benchmarking,plotting} ...

Combinatorial Optimization Project

positional arguments:
  {preprocessing,benchmarking,plotting}
    preprocessing       Launch preprocessing
    benchmarking        Launch benchmarking
    plotting            Plot graphs about benchmarking

optional arguments:
  -h, --help            show this help message and exit
```

### Preprocessing
```sh
$ python -m oc_project.main preprocessing -h
usage: main.py preprocessing [-h] -o PATH

optional arguments:
  -h, --help           show this help message and exit
  -o PATH, --out PATH  File path where you want to save preprocessing results.
```

### Benchmarking
```sh
$ python -m oc_project.main benchmarking -h
usage: main.py benchmarking [-h] -i PATH -o PATH

optional arguments:
  -h, --help           show this help message and exit
  -i PATH, --in PATH   File path containing preprocessing results.
  -o PATH, --out PATH  File path where you want to save benchmarking results.
```

### Plotting
```sh
$ python -m oc_project.main plotting -h    
usage: main.py plotting [-h] -i PATH -o PATH

optional arguments:
  -h, --help           show this help message and exit
  -i PATH, --in PATH   File path containing benchmarking results.
  -o PATH, --out PATH  Directory path where you want to save graphs.
```