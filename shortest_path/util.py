def get_arcs(path):
    "Given a path returns the arcs it is composed of."
    for index in range(len(path)-1):
        yield (path[index], path[index+1])


def filter_iterable(ll, excluded_items):
  return list(filter(lambda item: item not in excluded_items, ll))