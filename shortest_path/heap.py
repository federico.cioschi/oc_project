class HeapElement:
    def __init__(self, node, value):
        self.node = node
        self.value = value


class BinaryHeap:
    def __init__(self):
        self.L = 0
        self.heap = dict()
        self.pos = dict()
    
    def insert(self, node, value):
        self.L += 1
        self.heap[self.L] = HeapElement(node, value)
        self.pos[node] = self.L
        self.moveUp(self.L)

    def topNode(self):
        return self.heap[1].node

    def top(self):
        return self.heap[1].value
    
    def extractMin(self):
        root       = self.heap[1].node
        root_value = self.heap[1].value
        self.swap(1, self.L)
        del self.heap[self.L]
        del self.pos[root]
        self.L -= 1
        self.moveDn(1)
        return root, root_value
        
    def decreaseKey(self, node, value):
        p = self.pos[node]
        self.heap[p].value = value
        self.moveUp(p)
        
    def moveUp(self, p):
        stop = False
        while p!=1 and not stop:
            q = self.parent(p)
            if self.heap[q].value > self.heap[p].value:
                self.swap(p,q)
                p = q
            else:
                stop = True
    
    def moveDn(self, p):
        stop = False
        while self.left(p)<=self.L and not stop:
            if self.right(p)>self.L or self.heap[self.left(p)].value < self.heap[self.right(p)].value:
                q = self.left(p)
            else:
                q = self.right(p)
            if self.heap[p].value > self.heap[q].value:
                self.swap(p,q)
                p = q
            else:
                stop = True

    def swap(self, p, q):
        node_p, node_q = self.heap[p].node, self.heap[q].node
        self.heap[p], self.heap[q] = self.heap[q], self.heap[p]
        self.pos[node_p], self.pos[node_q] = self.pos[node_q], self.pos[node_p]
        
    
    def left(self, p):
        return 2*p
        
    def right(self, p):
        return 2*p+1
            
    def parent(self, p):
        return p//2
        
    def __str__(self):
        result =  "{}\n" +\
        "L = {}\n" +\
        "Heap = {}\n" +\
        "Positions = {}\n" +\
        "{}"
        result = result.format('='*80, self.L, [(self.heap[p].node, self.heap[p].value) for p in self.heap], self.pos, '='*80)
        return result
        





















        
    
    