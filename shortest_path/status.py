from enum import Enum

class Status(Enum):
    UNREACHED = 1
    LABELLED = 2
    SCANNED = 3