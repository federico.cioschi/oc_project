from oc_project.shortest_path.heap import BinaryHeap
from oc_project.shortest_path.status import Status

def build_path(pred, source, target, reverse=False):
    "Build path from 'pred' vector"
    path = [target]
    if source!=target:
        while True:
            predecessor = pred[path[-1]]
            path.append(predecessor)
            if predecessor == source:
                break
  
    return path[::-1] if not reverse else path[1:]
  

################################################################################

def dijkstra_C(G, source, target):
    """Implementazione slide 8 pacco C."""
    if source==target:
        return [source]
    O = set([source])
    E = set()
    w = 0
    y = {source: 0}
    pred = {source: source}
    
    def l(u,v):
        return G[pred[v]][v]['weight'] if u!=v else 0

    while len(O)!=0 and target not in E:
        j, theta = min([(v, l(pred[v],v) - y[v] + y[pred[v]]) for v in O], key=lambda couple: couple[1])
        O.remove(j)
        E.add(j)
        w += theta
        y[j] = w
        for k in O:
            y[k] = w
        for (j,k) in G.out_edges(j):
            if k not in E:
                if k in O:
                    if y[j] + G[j][k]['weight'] < y[pred[k]] + G[pred[k]][k]['weight']:
                        pred[k] = j
                else:
                    O.add(k)
                    y[k] = w
                    pred[k] = j
  
    return build_path(pred, source, target)



def dijkstra_source2all(G, source, stopping_criterion=lambda _: False):
    "Implementazione slide 4, pacco B"
    # Initialization
    c = lambda i, j: G[i][j]['weight']
    pred = [None for _ in range(len(G.nodes)+1)]
    dist = [None for _ in range(len(G.nodes)+1)]
    heap = BinaryHeap()
    for i in G.nodes:
        if i==source:
            heap.insert(i, 0)
            dist[i] = 0
        else:
            heap.insert(i, float('inf'))
            dist[i] = float('inf')

    # Core
    while heap.L>0:
        i, v = heap.extractMin()
        if stopping_criterion(i):
            break
        for (i,j) in G.out_edges(i):
            alt_dist = v + c(i, j)
            if alt_dist < dist[j]:
                heap.decreaseKey(j, alt_dist)
                dist[j] = alt_dist
                pred[j] = i
    
    return pred


def dijkstra_source2target(G, source, target):
    pred = dijkstra_source2all(G, source, stopping_criterion=lambda i: i==target)
    return build_path(pred, source, target)


def Astar(G, source, target):
    if source==target:
        return [source]
    h = lambda _: 0
    O = set([source])
    E = set()
    d = {source: 0}
    pred = dict()
    while len(O)!=0 and target not in E:
        j, _ = min([(v, d[v] + h(v)) for v in O], key=lambda couple: couple[1])
        O.remove(j)
        E.add(j)
        for k in G.successors(j):
            if k not in E:
                if k in O:
                    if d[k] > d[j] + G[j][k]['weight']:
                        d[k] = d[j] + G[j][k]['weight']
                        pred[k] = j
                else:
                    O.add(k)
                    d[k] = d[j] + G[j][k]['weight']
                    pred[k] = j
  
    return build_path(pred, source, target)


null_h_fun = lambda _: 0


def bidirectional_Astar(original_G, source, target, h_f=null_h_fun, h_r=null_h_fun, vertex_reaches=None):
    """This is an implementation of bidirectional A*.
    It can be used also as a bidirectional Dijkstra implementation if the bounding functions past as
    parameters return always 0.
    If `vertex_reaches` parameter is past, then vertex reaches pruning is applied.

    Pruning based on vertex reaches happens when a node `k` is considered as the successor of j,
    which is the last node extracted from the heap used in the current direction.

    In the case of A*, search on `k` is pruned if `r(k) < min(d(k), h(k))`, where `d(k)` represents
    the distance from the `source` to `k`, and `h(k)` is a lower bound on the distance from `k` to
    the `target`, provided by a proper potential function `h` past as parameter.

    In the case of Dijkstra, search on `k` is pruned if `k` has not been scanned in the opposite
    direction AND `r(k) < min(d(k), dist(k, t))`, where the distance `dist(k, t)` from `k` to the
    `target` is approximated, as lower bound, by the smallest not-permanent label in the opposite
    direction (i.e. the label on top of the heap used in the opposite direction).

    The stopping criterion used for A* was taken from section 5.1 "Symmetric Approach" of paper
    "Computing the Shortest Path: A* Search Meets Graph Theory" by Goldberg and Harrelson.
    """
    dijkstra = h_f==h_r==null_h_fun
    G_f = original_G;                       G_r = original_G.reverse(copy=False)
    d_f = {source: 0};                      d_r = {target: 0}
    key_f = lambda v: d_f[v]+h_f(v);        key_r = lambda v: d_r[v]+h_r(v)
    labeled_f = set([source]);              labeled_r = set([target])
    heap_f = BinaryHeap();                  heap_r = BinaryHeap()
    heap_f.insert(source, key_f(source));   heap_r.insert(target, key_r(target))
    scanned_f = set();                      scanned_r = set()
    pred_f = dict();                        pred_r = dict()
    mu = float('inf')
    mu_node = None

    while heap_f.L>0 and heap_r.L>0:
        if heap_f.top() <= heap_r.top():
            G, labeled, scanned, d, h, key, heap, pred = G_f, labeled_f, scanned_f, d_f, h_f, key_f, heap_f, pred_f
            scanned_opp, d_opp, heap_opp = scanned_r, d_r, heap_r
            j = heap_f.topNode() # node to scan
        else:
            G, labeled, scanned, d, h, key, heap, pred = G_r, labeled_r, scanned_r, d_r, h_r, key_r, heap_r, pred_r
            scanned_opp, d_opp, heap_opp = scanned_f, d_f, heap_f
            j = heap_r.topNode() # node to scan

        if dijkstra:
            if heap_f.top()+heap_r.top()>=mu:
                break
        else:
            if key(j) >= mu:
                break

        heap.extractMin() # extract the node to scan (j) from the heap
        labeled.remove(j)
        scanned.add(j)

        for k in G.successors(j):
            if k not in scanned:
                alt_dist = d[j] + G[j][k]['weight']

                # Reach pruning
                if vertex_reaches is not None:
                    if dijkstra:
                        if k not in scanned_opp and vertex_reaches[k] < min(alt_dist, heap_opp.top()):
                            continue
                    else:
                        if vertex_reaches[k] < min(alt_dist, h(k)):
                            continue

                if k in labeled:
                    if d[k] > alt_dist:
                        d[k] = alt_dist
                        heap.decreaseKey(k, key(k))
                        pred[k] = j
                else:
                    labeled.add(k)
                    d[k] = alt_dist
                    heap.insert(k, key(k))
                    pred[k] = j
            
                if k in scanned_opp:
                    alt_mu = d[k] + d_opp[k]
                    if alt_mu < mu:
                        mu = alt_mu
                        mu_node = k

    return {
        'shortest_path': build_path(pred_f, source, mu_node) + build_path(pred_r, target, mu_node, reverse=True) if mu_node is not None else None,
        'scanned': {
            'forward': scanned_f,
            'reverse': scanned_r
        },
        'labeled': {
            'forward': labeled_f,
            'reverse': labeled_r
        }
    }


def bidirectional_dijkstra(original_G, source, target, vertex_reaches=None):
    return bidirectional_Astar(original_G, source, target, h_f=null_h_fun, h_r=null_h_fun, vertex_reaches=vertex_reaches)