from oc_project.shortest_path.heap import BinaryHeap
from treelib import Tree
from oc_project.shortest_path.util import get_arcs, filter_iterable
from tqdm import tqdm
from oc_project.data_saving.data_saving import pickle_dump


def grow_partial_tree(G, source, epsilon, generate_tree=False):
    """This is the partial-tree growing procedure described in the technical report
    in section 5.2.2.
    More in detail, it is a Dijkstra implementation which follows this pseudo-code:
      ```
      insert pseudo-code slide 4, pacco B
      ```
    but it is modified in order to work as partial-tree growing procedure..
    Label management was taken from paper, pag. 2, second column."""

    # Initialization
    c       = lambda i, j: G[i][j]['weight']
    pred    = dict()
    f       = dict()
    dist    = dict()
    labeled = set()
    scanned = set()
    #status =  [Status.UNREACHED for _ in NODE_INDEXES]
    source_neighbours = set()
    inner_circle = set()
    outer_circle = set()
    heap = BinaryHeap()
    for i in G.nodes:
        if i==source:
            heap.insert(i, 0)
            dist[i] = 0
            labeled.add(i)
            inner_circle.add(i)
            f[i] = i
            #status[i] = Status.LABELLED
        else:
            heap.insert(i, float('inf'))
            dist[i] = float('inf')

    # Utilities
    def dist_from_inner_circle(j):
        d = 0
        while j not in inner_circle:
            i = pred[j]
            d += c(i, j)
            j = i
        return d

    def stop():
        for i in labeled:
            if not (i in outer_circle and dist_from_inner_circle(i) > epsilon):
                return False
        return True

    # Algorithm Core
    while heap.L>0 and not stop():
        i, v = heap.extractMin()
        labeled.remove(i)
        scanned.add(i)
        #status[i] = Status.SCANNED
        for (i,j) in G.out_edges(i):
            if i==source:
              source_neighbours.add(j)
            alt_dist = v + c(i, j)
            if alt_dist < dist[j]:
                heap.decreaseKey(j, alt_dist)
                dist[j] = alt_dist
                pred[j] = i
                f[j] = j if j in source_neighbours else f[pred[j]]
                labeled.add(j)
                #status[j] = Status.LABELLED
                if dist[j] - c(source, f[j]) <= epsilon:
                    outer_circle.discard(j)
                    inner_circle.add(j)
                else:
                    outer_circle.add(j)

    return {
      'source': source,
      'pred': pred,
      'dist': dist,
      'f' : f,
      'labeled': labeled,
      'scanned': scanned,
      'inner_circle': inner_circle,
      'outer_circle': outer_circle,
      'tree': create_tree(source, pred) if generate_tree else None
    }


def create_tree(source, pred):
    """Creates a treelib.Tree object according to the given `pred` dict.

    First of all the tree is created as a dictionary where a key represents
    a parent node and the value represents the list of its children.

    Then, the effective treelib.Tree object is created employing the recursive
    sub-procedure `tree_generation`.
    """

    def tree_generation(tree: Tree, dict_tree: dict, node: int, parent: int):
        tree.create_node(identifier=node, parent=parent)
        for child in dict_tree[node]:
            tree_generation(tree, dict_tree, child, node)

    dict_tree = {node:list() for node in pred}
    dict_tree[source] = list()
    for head in pred: # Given an arc (v,w): v is the tail and w is the head.
        tail = pred[head]
        dict_tree[tail].append(head)

    tree = Tree()
    tree_generation(tree, dict_tree, source, None)
    return tree


def update_arc_reaches_according_to_partial_tree(original_G_with_shortcuts, current_G, partial_tree_results, deleted_arcs, arc_reaches):
    """Updates arc reaches according to the result of the last
    `grow_partial_tree` execution.
    The behaviour of this funtion is described in sections 5.2.2
    and 5.2.3 of the technical report.

    Note: the dictionary referenced by label `arc_reaches` is updated
    by this function, but not returned. Callers should always mantain
    a reference to it in order to prevent data losss.

    Note: the treelib.Tree object does not mantain data on arc lengths
    and distances, which must be retrieved from graph `G` and `dist`
    array.
    """

    inner_circle      = partial_tree_results['inner_circle']
    labeled           = partial_tree_results['labeled']
    d                 = partial_tree_results['dist'].copy()
    tree              = partial_tree_results['tree']
    source            = partial_tree_results['source']
    height            = dict()
    source_in_penalty = in_penalty(original_G_with_shortcuts, source, current_G, arc_reaches)
    depth             = lambda v: d[v] + source_in_penalty
    l                 = lambda i, j: current_G[i][j]['weight']

    def get_descendants(node):
        paths_to_leaves = tree.subtree(node).paths_to_leaves()
        return set(node for path in paths_to_leaves for node in path)

    # Add pseudo-leaves
    for node in list(tree.nodes.keys()):
        identifier = -node # Pseudo-leaves have as identifier: parent.identifier * -1
        tree.create_node(identifier=identifier, parent=node)
        d[identifier] = d[node] + out_penalty(original_G_with_shortcuts, node, current_G, arc_reaches)

    # Calculate heights
    for node in inner_circle:
        descendants = get_descendants(node)
        if labeled & descendants == set():
            height[node] = max(d[leaf.identifier]-d[node] for leaf in tree.leaves(node))
        else:
            height[node] = float('inf')

    # update arc reaches for inner arcs
    for path in tree. subtree(source).paths_to_leaves():
        for (u,v) in get_arcs(path):
            if u in inner_circle and v in inner_circle:
                tree_relative_reach = min(depth(v), height[v]+l(u,v))
                if tree_relative_reach > arc_reaches[(u,v)]:
                    arc_reaches[(u,v)] = tree_relative_reach


def in_penalty(original_G_with_shortcuts, v, current_G, arc_reaches):
    "Definition of in-penalty: section 5.2.3 of the technical report."
    return max((arc_reaches[(u,v)] for u in original_G_with_shortcuts.predecessors(v) if (u,v) not in current_G.edges), default=0)


def out_penalty(original_G_with_shortcuts, v, current_G, arc_reaches):
    "Definition of out-penalty: section 5.2.3 of the technical report."
    return max((arc_reaches[(v,w)] for w in original_G_with_shortcuts.successors(v) if (v,w) not in current_G.edges), default=0)


def delete_arcs_with_low_reach(current_G, arc_reaches, deleted_arcs, epsilon):
    """Deletes arcs with low reach, according to epsilon, from current_G.

    Note: `current_G` and `deleted_arcs` are updated but not returned.
    Callers should mantain a reference to it in order to prevent data loss.
    Note: for now, only arcs are deleted, while vertices are not.
    """

    # Delete arcs
    for arc in list(current_G.edges().keys()):
        if arc_reaches[arc] < epsilon:
            u,v = arc
            current_G.remove_edge(u,v)
            deleted_arcs.add(arc)

    # Delete nodes without arcs
    for node in list(current_G.nodes):
        if current_G.degree(node)==0:
            current_G.remove_node(node)


def partial_tree_step(original_G_with_shortcuts, current_G, arc_reaches, deleted_arcs, epsilon):
    """Performs the partial tree step as described by section 5.2 of the
    technical report.

    Low reach arcs are removed from `current_G` according to value of epsilon.

    `arc_reaches` and `deleted_arcs` structures are updated but not returned.
    Callers should mantain a reference to them in order to prevent data loss.
    """
    for source in tqdm(current_G.nodes(), desc="[-->] partial-tree-step", ncols=80):
        gpt_results = grow_partial_tree(current_G, source, epsilon, generate_tree=True)
        update_arc_reaches_according_to_partial_tree(original_G_with_shortcuts, current_G, gpt_results, deleted_arcs, arc_reaches)
    delete_arcs_with_low_reach(current_G, arc_reaches, deleted_arcs, epsilon)


def delete_overlapping_nodes(line):
    """When a line represents a circle it may happen that some
    nodes overlaps in the line calculated by `get_lines()`.
    This function delete overlaps."""
    result = list()
    for node in line:
        result.append(node)
        if node in result[:-1]:
            return result
    return result


def is_circular(line):
    return line[0]==line[-1]


def get_lines(G):
    "NB: each node has itself as successor and as predecessor!"
    bypassable_1way = set()
    bypassable_2way = set()
    for v in G.nodes:
        out_degree = G.out_degree(v)
        in_degree = G.in_degree(v)
        if out_degree>2 or in_degree>2:
            continue
        else:
            successors = list(G.successors(v))
            predecessors = list(G.predecessors(v))
            if out_degree==1 and in_degree==1 and successors[0]!=predecessors[0]:
                bypassable_1way.add(v)
                #print("Added {}".format(v))
            elif out_degree==2 and in_degree==2 and successors[0] in predecessors and successors[1] in predecessors:
                bypassable_2way.add(v)
                # NB: one should check successors!=predecessors, but is not necessary because in DiGraphs a vertex cannot have 2 outgoing or incoming edges toward/from the same other node.
  
    lines_1way = list()
    lines_2way = list()

    # build 1-way lines
    while len(bypassable_1way)!=0:
        v = bypassable_1way.pop()
        predecessors = list(G.predecessors(v))
        successors = list(G.successors(v))
        current_sx = predecessors[0]
        current_dx = successors[0]
        line_sx = list()
        line_dx = list()

    
        while current_sx in bypassable_1way:
            bypassable_1way.remove(current_sx)
            line_sx.append(current_sx)
            predecessors = list(G.predecessors(current_sx))
            current_sx = predecessors[0]
        line_sx.append(current_sx)


        while current_dx in bypassable_1way:
            bypassable_1way.remove(current_dx)
            line_dx.append(current_dx)
            successors = list(G.successors(current_dx))
            current_dx = successors[0]
        line_dx.append(current_dx)

        result = line_sx[::-1] + [v] + line_dx
        result = delete_overlapping_nodes(result)
        if not is_circular(result):
            lines_1way.append(result)


    # build 2-way lines
    while len(bypassable_2way)!=0:
        v = bypassable_2way.pop()
        v_successors = list(G.successors(v))
        current_dir1 = v_successors[0]
        current_dir2 = v_successors[1]

        line_dir1 = list()
        line_dir2 = list()

        prec = v
        while current_dir1 in bypassable_2way:
            bypassable_2way.remove(current_dir1)
            line_dir1.append(current_dir1)
            successors = filter_iterable(G.successors(current_dir1), [prec])
            prec = current_dir1
            current_dir1 = successors[0]
        line_dir1.append(current_dir1)

        prec = v
        while current_dir2 in bypassable_2way:
            bypassable_2way.remove(current_dir2)
            line_dir2.append(current_dir2)
            successors = filter_iterable(G.successors(current_dir2), [prec])
            prec = current_dir2
            current_dir2 = successors[0]
        line_dir2.append(current_dir2)


        if line_dir2[0] in v_successors:
            result = line_dir1[::-1] + [v] + line_dir2
        else:
            result = line_dir2[::-1] + [v] + line_dir1

        result = delete_overlapping_nodes(result)
        if not is_circular(result):
            lines_2way.append(result)
  
    return lines_1way, lines_2way


def get_closer_to_median_index(G, line, two_way=False):
    """Given a line returns the index of the closest element
    to the median of the path.

    See section 5.3 and 5.3.1 of the technical report."""
    dist = [None for _ in line]
    acc = 0
    index = 0

    # Calculate vertices' distances from the beginning of line
    for arc in get_arcs(line):
        dist[index] = acc
        acc += max_arc_len(G, arc, two_way)
        dist[index+1] = acc
        index += 1

    line_len = acc
    median = line_len/2

    closest_tm_index = sorted([index for index in range(len(line))], key=lambda i: abs(dist[i]-median))[0]

    return closest_tm_index


def max_arc_len(G, arc, two_way):
    u, v = arc[0], arc[1]
    return max(arc_len(G, (u,v)), arc_len(G, (v,u))) if two_way else arc_len(G, (u,v))


def arc_len(G, arc):
    u, v = arc[0], arc[1]
    return G[u][v]['weight']


def max_line_len(G, line, two_way):
    """Returns the length of a line. If it is a two-way line
    the length is calculated taking the maximum length of the
    two arcs in each segment.

    See the end of section 5.3.1 of the technical report."""
    return sum(max_arc_len(G, arc, two_way) for arc in get_arcs(line))


def line_len(G, line):
    return sum(arc_len(G, arc) for arc in get_arcs(line))


def create_shortcuts(original_G_with_shortcuts, current_G, line, _lambda, deleted_arcs, arc_reaches, shortcuts, two_way=False):
    """Given a line, this function creates recursively shortcuts to bypass line's
    vertices according to the `_lambda` bound.
    Structures `deleted_arcs`, `arc_reaches` and `shortcuts` are updated accordingly.

    If a line is bidirectional the decision whether to create the shortcut is taken
    respect to `max_line_length`, which is calculated taking the maximum length of
    the two arcs in each segment. However, when a shortcut is created in one
    direction, its length is specifically equal to cumulative length of the bypassed
    arcs in that direction.

    Note: during the recursive creation of the shortcuts, most of them are immediately
    eliminated and so addedd to `deleted_arcs`.
    """
    k = len(list(get_arcs(line)))
    u, w = line[0], line[-1]
    max_line_length = max_line_len(current_G, line, two_way)
    line_length = line_len(current_G, line)
    rev_line = line[::-1]
    rev_line_length = line_len(current_G, rev_line) if two_way else None

    def create_single_shortcut(u, w, weight, line):
        if (u, w) in original_G_with_shortcuts.edges:
            if (u, w) in shortcuts:
                existent_weight, existent_line = shortcuts[(u, w)]
                if existent_weight < weight:
                    weight = existent_weight
                    line = existent_line
            elif original_G_with_shortcuts[u][w]['weight'] < weight:
                weight = original_G_with_shortcuts[u][w]['weight']
                line = [u,w]

        shortcuts[(u,w)] = (weight, line)
        current_G.add_edge(u, w, weight=weight)
        original_G_with_shortcuts.add_edge(u, w, weight=weight)
        if (u,w) not in arc_reaches:
            arc_reaches[(u,w)] = 0

    def remove_bypassed_arcs_and_update_reaches(u,v,w, remove_node):
        arc_reaches[(u,v)] = arc_len(current_G, (u,v)) + out_penalty(original_G_with_shortcuts, v, current_G, arc_reaches)
        arc_reaches[(v,w)] = arc_len(current_G, (v,w)) + in_penalty(original_G_with_shortcuts, v, current_G, arc_reaches)
        current_G.remove_edge(u,v)
        current_G.remove_edge(v,w)
        if remove_node:
            current_G.remove_node(v)
        deleted_arcs.add((u,v))
        deleted_arcs.add((v,w))

    v = None
    if k>2:
        closest_tm_index = get_closer_to_median_index(current_G, line, two_way=two_way)
        v = line[closest_tm_index]
        if (u,v) not in current_G.edges:
            create_shortcuts(original_G_with_shortcuts, current_G, line[:closest_tm_index+1], _lambda, deleted_arcs, arc_reaches, shortcuts, two_way=two_way)
        if (v,w) not in current_G.edges:
            create_shortcuts(original_G_with_shortcuts, current_G, line[closest_tm_index:],   _lambda, deleted_arcs, arc_reaches, shortcuts, two_way=two_way)
    else:
        assert(k==2)
        v = line[1]

    if max_line_length<_lambda:
        create_single_shortcut(u, w, line_length, line)
        remove_bypassed_arcs_and_update_reaches(u,v,w, remove_node=not two_way)
        if two_way:
            create_single_shortcut(w, u, rev_line_length, rev_line)
            remove_bypassed_arcs_and_update_reaches(w,v,u, remove_node=True)


def shortcut_step(original_G_with_shortcuts, current_G, arc_reaches, deleted_arcs, shortcuts, _lambda):
    lines_1way, lines_2way = get_lines(current_G)

    for line in lines_1way:
        create_shortcuts(original_G_with_shortcuts, current_G, line, _lambda, deleted_arcs, arc_reaches, shortcuts, two_way=False)

    for line in lines_2way:
        create_shortcuts(original_G_with_shortcuts, current_G, line, _lambda, deleted_arcs, arc_reaches, shortcuts, two_way=True)


def main_phase(G, epsilon1, alpha, arc_reaches, shortcuts):
    """

    """
    current_G = G.copy()
    original_G_with_shortcuts = G.copy()
    deleted_arcs = set()
    current_epsilon = epsilon1

    iteration = 0

    print("[+] Starting main-phase with epsilon1={} and alpha={}".format(epsilon1, alpha), flush=True)

    _lambda = epsilon1/2
    shortcut_step(original_G_with_shortcuts, current_G, arc_reaches, deleted_arcs, shortcuts, _lambda)
    print("[+] shortcut-step 0 with lambda={} done! Now graph has {} nodes and {} arcs".format(_lambda, len(current_G.nodes), len(current_G.edges)), flush=True)

    iteration += 1
    while len(current_G.nodes)>0:
        print("\n[+] Starting iteration {} with epsilon={}".format(iteration, current_epsilon), flush=True)
        partial_tree_step (original_G_with_shortcuts, current_G, arc_reaches, deleted_arcs, current_epsilon)
        print("", flush=True, end='')
        print("[+] partial-tree-step {} done! Now graph has {} nodes and {} arcs".format(iteration, len(current_G.nodes), len(current_G.edges)), flush=True)

        _lambda = current_epsilon*alpha/2
        shortcut_step     (original_G_with_shortcuts, current_G, arc_reaches, deleted_arcs, shortcuts, _lambda)
        print("[+] shortcut-step {} with lambda={} done! Now graph has {} nodes and {} arcs".format(iteration, _lambda, len(current_G.nodes), len(current_G.edges)), flush=True)

        current_epsilon *= alpha
        iteration += 1

    return original_G_with_shortcuts


def refinement_phase(G_with_shortcuts, shortcuts):
    "TODO"
    pass


def calculate_vertex_reaches(G_with_shortcuts, arc_reaches):
    """Calculates and returns `vertex_reaches` according to section 5.2.1
    of the technical report."""
    r = lambda a,b: arc_reaches[(a,b)]
    vertex_reaches = dict()

    for v in G_with_shortcuts.nodes:
        x_prime = max((x for x in G_with_shortcuts.predecessors(v)),                   key=lambda x: r(x,v), default=None)
        y_prime = max((y for y in G_with_shortcuts.successors(v) if y!=x_prime),       key=lambda y: r(v,y), default=None)
        if x_prime is not None and y_prime is not None:
            delta_1 = min(r(x_prime, v), r(v, y_prime))
        elif x_prime is not None:
            delta_1 = x_prime
        elif y_prime is not None:
            delta_1 = y_prime
        else:
            delta_1 = None

        y_prime = max((y for y in G_with_shortcuts.successors(v)),                     key=lambda y: r(v,y), default=None)
        x_prime = max((x for x in G_with_shortcuts.predecessors(v) if x!=y_prime),     key=lambda x: r(x,v), default=None)
        if x_prime is not None and y_prime is not None:
            delta_2 = min(r(x_prime, v), r(v, y_prime))
        elif x_prime is not None:
            delta_2 = x_prime
        elif y_prime is not None:
            delta_2 = y_prime
        else:
            delta_2 = None

        if delta_1 is not None and delta_2 is not None:
            vertex_reaches[v] = max(delta_1, delta_2)
        elif delta_1 is not None:
            vertex_reaches[v] = delta_1
        elif delta_2 is not None:
            vertex_reaches[v] = delta_2

    return vertex_reaches


def preprocess(G, epsilon1, alpha):
    arc_reaches = {arc:0 for arc in G.edges}
    shortcuts = dict()

    print("[+] Preprocessing graph with {} nodes and {} arcs".format(len(G.nodes), len(G.edges)), flush=True)

    G_with_shortcuts = main_phase(G, epsilon1, alpha, arc_reaches, shortcuts)

    vertex_reaches = calculate_vertex_reaches(G_with_shortcuts, arc_reaches)

    # refinement_phase(G_with_shortcuts, shortcuts)

    print("[+] Preprocessing finished!", flush=True)

    results = {
        "G_with_shortcuts"  : G_with_shortcuts,
        "shortcuts"         : shortcuts,
        "arc_reaches"       : arc_reaches,
        "vertex_reaches"    : vertex_reaches
    }

    return results