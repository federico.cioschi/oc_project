import networkx as nx
import gzip
import urllib.request


def decompress(archive_data):
    return gzip.decompress(archive_data)


def DIMACS_builder(lines):
    G = nx.DiGraph()
    for line in lines:
        splitted_line = line.split(' ')
        _type = splitted_line[0]
        if _type == 'a':
            u, v, weight = int(splitted_line[1]), int(splitted_line[2]), int(splitted_line[3])
            G.add_edge(u, v, weight=weight)
    return G


def TIGER_builder(lines):
    """A node with index 0 represents a problem. Since in this case node
    numeration starts from zero, each node index is increased of 1.

    Moreover, graphs are undirected, so for each arc add also its inverse.

    Note: in the input file coordinates are longitude-latitude couples
    expressed in decimal degrees multiplied for 1000000. So during acquisition
    they are rescaled.
    """
    G = nx.DiGraph()
    coord = dict()

    n_nodes = int(next(lines))
    for _ in range(n_nodes):
        line_split = next(lines).split(' ')
        node, long, lat = int(line_split[0])+1, int(line_split[1])/1000000, int(line_split[2])/1000000
        coord[node] = (long, lat)

    n_arcs = int(next(lines))
    for _ in range(n_arcs):
        first_line_split = next(lines).split(' ')
        second_line_split = next(lines).split(' ')
        u, v = int(first_line_split[0])+1, int(first_line_split[1])+1
        weight = float(second_line_split[1])
        if u!=v:
            G.add_edge(u, v, weight=weight)
            G.add_edge(v, u, weight=weight)

    return G, coord


def build_graph_from_lines(lines, builder):
    return builder(lines)


def get_lines_from_string(string):
    return (line for line in string.split('\n'))


def get_lines_from_file(file_path, decomp):
    with open(file_path, 'rb') as file:
        data = file.read()
        if decomp:
            print("[+] Decompressing...")
            data = decompress(data).decode('utf-8')
        else:
            data = data.decode('utf-8')
        return get_lines_from_string(data)


def graph_cache(function):
    cache = dict()
    def wrapper(url):
        if url not in cache:
            cache[url] = function(url)
        return cache[url].copy()
    return wrapper


#@graph_cache
def build_graph_from_file(path, builder, decomp=True):
    print("[+] Getting graph from '{}'".format(path))
    lines = get_lines_from_file(path, decomp=decomp)
    print("[+] Building graph...")
    result = build_graph_from_lines(lines, builder)
    print("[+] Graph built")
    return result


#@graph_cache
def download_and_build_graph(url, builder, decomp=True):
    print("[+] Downloading data from '{}' ...".format(url))
    archive_data = urllib.request.urlopen(url).read()
    if decomp:
        print("[+] Decompressing...")
        data = decompress(archive_data).decode('utf-8')
    else:
        data = archive_data.decode('utf-8')
    lines = get_lines_from_string(data)
    print("[+] Building graph...")
    result = build_graph_from_lines(lines, builder)
    print("[+] Graph built!")
    return result

