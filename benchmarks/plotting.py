from matplotlib import pyplot as plt
from scipy import optimize

import numpy as np
import os


def segments_fit(X, Y, count):
    "From: https://gist.github.com/ruoyu0088/70effade57483355bbd18b31dc370f2a"
    xmin = X.min()
    xmax = X.max()

    seg = np.full(count - 1, (xmax - xmin) / count)

    px_init = np.r_[np.r_[xmin, seg].cumsum(), xmax]
    py_init = np.array([Y[np.abs(X - x) < (xmax - xmin) * 0.01].mean() for x in px_init])

    def func(p):
        seg = p[:count - 1]
        py = p[count - 1:]
        px = np.r_[np.r_[xmin, seg].cumsum(), xmax]
        return px, py

    def err(p):
        px, py = func(p)
        Y2 = np.interp(X, px, py)
        return np.mean((Y - Y2)**2)

    r = optimize.minimize(err, x0=np.r_[seg, py_init], method='Nelder-Mead')
    return func(r.x)


def plot_scanned_nodes_function(scanned_nodes_points, algoname, xlabel, plot_points=True):

    scanned_nodes_points = sorted(scanned_nodes_points, key=lambda e: e[0])
    x_values = np.array([x for x,_ in scanned_nodes_points])
    y_values = np.array([y for _,y in scanned_nodes_points])

    px, py = segments_fit(x_values, y_values, 20)

    if plot_points:
        plt.plot(x_values, y_values, '.')
    plt.plot(px, py, '-', label=algoname, linewidth=3 if plot_points else 2)
    plt.xlabel(xlabel)
    plt.ylabel('Scanned vertices (forward and backward)')


def plot_graphs(algos, global_benchmarks_results, out_path):
    os.makedirs(out_path, exist_ok=True)

    for algolabel, algoname in algos:
        algo_results = global_benchmarks_results[algolabel]
        print("[+] {}".format(algoname))

        print("[+] Plotting scanned nodes function based on the arcs number domain...")
        plot_scanned_nodes_function(algo_results['scanned_nodes_function']['arcs_num_domain'], algoname=algoname, xlabel='number of arcs')
        plt.legend()
        saving_path = os.path.join(out_path, "{}_{}.png".format( algolabel, "arcs_num_domain"))
        plt.savefig(saving_path)
        print("[+] Saved in '{}'".format(saving_path))
        plt.clf()

        print("[+] Plotting scanned nodes function based on the path length domain...")
        plot_scanned_nodes_function(algo_results['scanned_nodes_function']['path_len_domain'], algoname=algoname, xlabel='path length [meters]')
        plt.legend()
        saving_path = os.path.join(out_path, "{}_{}.png".format(algolabel, "path_len_domain"))
        plt.savefig(saving_path)
        print("[+] Saved in '{}'".format(saving_path))
        plt.clf()
        print()

    print("[+] Plotting global results based on arcs number domain...")
    for algolabel, algoname in algos:
        algo_results = global_benchmarks_results[algolabel]
        plot_scanned_nodes_function(algo_results['scanned_nodes_function']['arcs_num_domain'], algoname=algoname, xlabel='number of arcs', plot_points=False)
    plt.legend()
    saving_path = os.path.join(out_path, "{}_{}.png".format("general", "arcs_num_domain"))
    plt.savefig(saving_path)
    print("[+] Saved in '{}'".format(saving_path))
    plt.clf()
    print()

    print("[+] Plotting global results based on path length domain...")
    for algolabel, algoname in algos:
        algo_results = global_benchmarks_results[algolabel]
        plot_scanned_nodes_function(algo_results['scanned_nodes_function']['path_len_domain'], algoname=algoname, xlabel='path length [meters]', plot_points=False)
    plt.legend()
    saving_path = os.path.join(out_path, "{}_{}.png".format("general", "path_len_domain"))
    plt.savefig(saving_path)
    print("[+] Saved in '{}'".format(saving_path))
    plt.clf()
    print()