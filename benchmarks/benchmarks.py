import networkx as nx

from oc_project.shortest_path import algorithms
from oc_project.shortest_path.util import get_arcs
from math import cos, asin, sqrt, radians, sin
from tqdm import tqdm


def scanned_count(algo_results):
    return sum(len(algo_results['scanned'][direction]) for direction in ['forward', 'reverse'] )


def substitute_shortcuts_closure(path, shortcuts):
    res = substitute_shortcuts(path, shortcuts)
    while True:
        newRes = substitute_shortcuts(res, shortcuts)
        if res==newRes:
            break
        else:
            res=newRes
    return res


def substitute_shortcuts(path, shortcuts):
    result = list()
    result.append(path[0])
    for (u, v) in get_arcs(path):
        if (u, v) in shortcuts:
            _, line = shortcuts[(u, v)]
            for node in line[1:]:
                result.append(node)
        else:
            result.append(v)
    return result


def path_len(G, path):
    c = lambda u,v : G[u][v]['weight']
    return sum(c(u,v) for (u,v) in get_arcs(path))


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    From: https://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points/4913653#4913653
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r


def generate_h_func(target, coord):
    target_long, target_lat = coord[target]
    def h(node):
        node_long, node_lat = coord[node]
        return haversine(node_long, node_lat, target_long, target_lat)*1000
    return h


def test_algo(algo, G_with_shortcuts, original_G, shortcuts, source, actual_paths, use_potential_functions=False, coord=None, vertex_reaches=None):

    benchmars_results = {
        'scanned_nodes_function': {
            'arcs_num_domain': list(),
            'path_len_domain': list()
        },
        'failed_searches': list()
    }

    use_preprocessing = vertex_reaches is not None

    for target in tqdm(actual_paths, desc="[+] Benchmarking {}{}".format(algo.__name__, " with reach pruning and shortcuts" if use_preprocessing else "" ) ):
        if target!=source:
            actual_path = actual_paths[target]

            kwargs = dict()
            if use_potential_functions:
                kwargs['h_f'] = generate_h_func(target, coord)
                kwargs['h_r'] = generate_h_func(source, coord)
            if use_preprocessing:
                kwargs['vertex_reaches'] = vertex_reaches
                test_G = G_with_shortcuts
            else:
                test_G = original_G

            algo_results = algo(test_G, source, target, **kwargs)
            algo_shortest_path = algo_results['shortest_path']
            if algo_shortest_path is None:
                benchmars_results['failed_searches'].append((source, target, None))
            else:
                if use_preprocessing:
                    algo_shortest_path = substitute_shortcuts_closure(algo_shortest_path, shortcuts)
                if actual_path!=algo_shortest_path:
                    benchmars_results['failed_searches'].append((source, target, path_len(original_G, algo_shortest_path) - path_len(original_G, actual_path)))
                else:
                    benchmars_results['scanned_nodes_function']['arcs_num_domain'].append(  (len(algo_shortest_path),                   scanned_count(algo_results)) )
                    benchmars_results['scanned_nodes_function']['path_len_domain'] .append( (path_len(original_G, algo_shortest_path),  scanned_count(algo_results)) )
    
    if len(benchmars_results['failed_searches'])>0:
        print("[-] {}: {} failed search(es) --> {}".format(algo.__name__, len(benchmars_results['failed_searches']), benchmars_results['failed_searches']))

    return benchmars_results


def benchmarks(G, preprocessing_results, coord):

    G_with_shortcuts = preprocessing_results['G_with_shortcuts']
    vertex_reaches = preprocessing_results['vertex_reaches']
    shortcuts = preprocessing_results['shortcuts']

    global_benchmark_results = dict()

    SOURCE = 1

    actual_paths = nx.shortest_path(G, SOURCE, weight='weight') # generate actual paths form SOURCE to all destinations

    global_benchmark_results['bid_dijkstra'] = \
        test_algo(algorithms.bidirectional_dijkstra, G_with_shortcuts, G, shortcuts, SOURCE, actual_paths)

    global_benchmark_results['bid_astar'] = \
        test_algo(algorithms.bidirectional_Astar, G_with_shortcuts, G, shortcuts, SOURCE, actual_paths, use_potential_functions=True, coord=coord)

    global_benchmark_results['bid_dijkstra_with_preprocessing'] = \
        test_algo(algorithms.bidirectional_dijkstra, G_with_shortcuts, G, shortcuts, SOURCE, actual_paths, vertex_reaches=vertex_reaches)

    global_benchmark_results['bid_astar_with_preprocessing'] = \
        test_algo(algorithms.bidirectional_Astar, G_with_shortcuts, G, shortcuts, SOURCE, actual_paths, use_potential_functions=True, coord=coord, vertex_reaches=vertex_reaches)

    return global_benchmark_results
