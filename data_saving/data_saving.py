import pickle
import os

from datetime import datetime

def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


def pickle_dump(data, out_path):
    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    with open(out_path, 'wb') as f:
        pickle.dump(data, f)
        print("[+] Data saved in {}".format(out_path))


def pickle_load(path):
    with open(path, 'rb') as f:
        return pickle.load(f)

